class Calculator < ApplicationRecord
	def self.add(input)
	    if input.empty?
	      	0
	    else
	     	numbers = extract_numbers(input)
	     	ensure_no_negatives(numbers)
	     	numbers.sum
	    end
	end


	def self.extract_numbers(input)
	    delimiter = extract_delimiter(input)
	    numbers = input.split(delimiter).map(&:to_i)
	end
	
	def self.extract_delimiter(input)
	    if input.start_with?("//")
	      	delimiter = input[2]
	      	if delimiter == '['
	        	delimiter = input.match(/\[([^\[\]]+)\]/)[1]
	      	end
	      	input.slice!(0..3)
	      	delimiter
	    else
	     	/[,\n]/
	    end
	end

	def self.ensure_no_negatives(numbers)
	    negatives = numbers.select { |num| num.negative? }
	    raise "Negatives not allowed: #{negatives.join(', ')}" unless negatives.empty?
	end
end
