require 'rails_helper'

RSpec.describe Calculator, type: :model do
 
  describe ".add" do
    it "returns 0 if empty string" do
      expect(Calculator.add("")).to eq(0)
    end

    it "returns the number for a single number" do
      expect(Calculator.add("5")).to eq(5)
    end

    it "returns the sum of two numbers separated by comma" do
      expect(Calculator.add("3,5")).to eq(8)
    end

    it "returns the sum of multiple numbers separated by comma" do
      expect(Calculator.add("1,2,3,4,5")).to eq(15)
    end

    it "ignores non-numeric characters" do
      expect(Calculator.add("1,2,three,4,five")).to eq(7)
    end

    it "handles new lines between numbers" do
      expect(Calculator.add("1\n2,3")).to eq(6)
    end

    it "supports custom delimiters" do
      expect(Calculator.add("//;\n1;2")).to eq(3)
    end

    it "raises an error for negative numbers" do
      expect { Calculator.add("1,-2,3,-4") }.to raise_error("Negatives not allowed: -2, -4")
    end
  end
end
